#pragma once

#include "types.h"
#include <time.h>

#ifdef __cplusplus
extern "C"
{
#endif
#define MAX_CHANNEL_LINES 7
#define MAX_MA_DIR 4

  typedef struct ChannelLines
  {
    int8_t type;
    dir_t slp_up;
    dir_t slp_dn;
  } ChannelLines;

  typedef struct Channels
  {
    dir_t cur_dir;
    time_t tm;
    ChannelLines cl[MAX_CHANNEL_LINES];
  } Channels;

  typedef struct MaxMin
  {
    time_t tm_max;
    time_t tm_min;
    price_t hp_max;
    price_t hp_min;
  } MaxMin;

  typedef struct MaxMin MaxMinArray[MAX_MA_DIR];

  typedef dir_t MaDirArray[MAX_MA_DIR];
  typedef dir_t IsBigNeedle[MAX_MA_DIR];

  extern void init_channels(Channels* cl);
  extern dir_t channel_dir_changed(Channels* cl);
  extern dir_t get_channel_dir(const Channels* cl,
                               const MaDirArray mad,
                               const IsBigNeedle bn);
  extern void print_channel(Channels* cl);
  extern void get_dirs_in_mma(const MaxMinArray mma,
                              MaDirArray mda,
                              IsBigNeedle bn,
                              const price_t hp_now,
                              const time_t tm_now);
  extern void get_max_min_in_mma(const MaxMinArray mma,
                                 price_t* hp_max,
                                 price_t* hp_min);
  extern int8_t nice_channel_dir(const Channels* cl, const MaDirArray mad);
  extern dir_t pre_opposite(const Channels* cl,
                            const MaDirArray mda,
                            const dir_t cd);

#ifdef __cplusplus
}
#endif
