#pragma once
#include <netdb.h>

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct UdpClient
  {
    int sockfd;
    struct addrinfo hints;
    struct addrinfo* servinfo;
    struct addrinfo* p;
  } UdpClient;

  extern UdpClient* init_udp_client(const char* addr, const char* port);
  extern void close_udp_client(UdpClient* uc);
  extern int udpc_send_data(UdpClient* uc, const void* data, uint16_t size);
  extern int udpc_send_message(UdpClient* uc, const char* msg);

#ifdef __cplusplus
}
#endif
