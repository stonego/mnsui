#pragma once
#include "types.h"
#include <ctime>
#include <cstring>

namespace Trade {

struct Wave
{
  dir_t dir = 0;            // a     // -1, 0, 1
  dir_t prev_dir = 0;       // b
  dir_t spot_dir = 0;       // c
  dir_t solved_cls_spp = 0; // d
  dir_t solved_vss_spp = 0; // e
  dir_t spp_cls_slope;      // f
  dir_t spp_vss_slope;      // g
  int8_t spp_cls_type;      // h
  int8_t spp_vss_type;      // i
  bool have_cls_spp = 0;    // j
  bool have_vss_spp = 0;    // k

  void reset() noexcept { memset((void*)this, 0, sizeof(Wave)); }
  void print(const dir_t mid, const dir_t ein, const int64_t idx) noexcept;
  bool good_score(const dir_t d) noexcept;
  bool solved_dir(const dir_t d) noexcept;
  bool half_solved_dir() noexcept;
  dir_t resolve_no_cls_vss_to_dir(const time_t tm_vss_near,
                                  const time_t tm_vss_far,
                                  const time_t t_now,
                                  const dir_t vss_dirp) noexcept;
};

} // namespace Trade