#pragma once
#include "dirs.hpp"
#include "wave.hpp"
#include <memory.h>
#include <time.h>

namespace Trade {

struct WaitingForWrong
{
  bool fs_work = false;
  dir_t g2_dir = 0;
  uint8_t to = 0;
  int8_t g2_type = 0;
  dir_t fs_dir = 0;
  int8_t fs_type = 0;
  dir_t cls_dir = 0;
  dir_t vss_dir = 0;
  time_t fs_time = 0;
  time_t tm_cls_near = 0;
  time_t tm_cls_far = 0;
  time_t tm_vss_near = 0;
  time_t tm_vss_far = 0;
  time_t max_amb_time = 0;
  time_t min_g2_time = 0;
  time_t g2_time = 0;
  price_t fs_hpx = 0;
  price_t g2_hpx = 0;

  void reset() noexcept
  {
    bool back_fs_work = fs_work;
    memset((void*)(this), 0, sizeof(WaitingForWrong));
    fs_work = back_fs_work;
  }
  void print(const time_t tm, const price_t hp_now) noexcept;
  bool valied() noexcept;
  bool far_more_2_hours(const time_t tm) noexcept;
  bool in_far_timer(const time_t tm) noexcept;
  uint8_t timer_out(const time_t tm) noexcept;
  void init_time_out(const time_t tm) noexcept;
  bool timer_outed(const time_t tm) noexcept;
  int64_t now_timer(const dir_t d, const time_t tm) noexcept;
  int8_t is_not_good_for(const dir_t d, const time_t tm) const noexcept;
  time_t get_timer_by_3same(const time_t t_now) noexcept;
  bool good_g2_and_vss(const dir_t vss_dir, const time_t t_now) noexcept;
  bool pass_one_go_two(const time_t t_now) noexcept;
  time_t get_timer(const Wave& w, const dir_t d, const time_t t_now) noexcept;
  time_t get_weak_timer(const Wave& w,
                        const StageDirs& dirs,
                        const dir_t d,
                        const time_t t_now) noexcept;
  time_t get_abs_timer(const Wave& w,
                       const dir_t d,
                       const time_t t_now) noexcept;
  time_t get_abs_weak_timer(const Wave& w,
                            const StageDirs& dirs,
                            const dir_t d,
                            const time_t t_now) noexcept;
  bool g2_timer_outed(const time_t t_now) const noexcept;
  bool fs_work_changed(const time_t t_now) noexcept;
  bool fs_is_intered() const noexcept;
  bool fs_good_for_dir(const dir_t d) const noexcept;
};

} // namespace Trade