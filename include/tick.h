#pragma once

#define _LARGEFILE64_SOURCE 1

#include <memory.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef MAX_PATH
#define MAX_PATH 1024
#endif // MAX_PATH

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#ifndef TICK_DATA_BINARY_PRICE_TIME
#define TICK_DATA_BINARY_PRICE_TIME
  typedef struct Tick
  {
    double ftime;
    double price;
  } Tick;

  typedef Tick* TICK_DATA;
#endif // TICK_DATA_BINARY_PRICE_TIME

  TICK_DATA load_tick_bin(const char* filepath,
                           size_t* size,
                           const int64_t spos,
                           const int64_t epos);

  TICK_DATA load_all_tick_bin(const char* filepath, size_t* size);

#ifdef __cplusplus
}
#endif // __cplusplus
