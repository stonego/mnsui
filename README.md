# Build 

## Gui Desktop For Linux(Ubuntu/Cenos/Debian ...)

The recommended GUI is xfce
```bash
sudo apt-get update
sudo apt-get install xfce4 xfce4-session
sudo apt install xinit
startx
```
## Install Qt 6

### OpenGL
- ubuntu
```bash
sudo apt-get install libgles2-mesa-dev
```

### Qt 6
- Download online installer [ https://www.qt.io/download-qt-installer-oss ], go to the Qt website -> Downloads for open source users -> Download the Qt Online Installer -> Qt Online Installer 

Or 

- ubuntu apt install
```bash
sudo apt install qt6-base-dev
```

## C++17 Or C++20 

Recommended compiler are gcc/g++ 13 and clang/clang++ 17

Modify the line in CMakeLists.txt

```bash
set(CMAKE_CXX_STANDARD 20)
```

Or

```bash
set(CMAKE_CXX_STANDARD 17)
```


## Get The Source Code


```bash
git clone https://gitee.com/stonego/mnsui.git
cd mnsui
```


## THe Directories

### Set The Qt6 Directory
Change the line to your Qt6 Directory in CMakeLists.txt
```bash
set(Qt6_DIR /home/mycat/Qt/6.6.2/gcc_64/lib/cmake/Qt6)
```
BUT if you are going to using Qt Creator, please remove this line

### Set Your Installation Directory
Set the **MU** in CMakeLists.txt
```bash
# set (MU /usr/local)
set (MU /ssd1/mycat/usr)
```

## Using gcc or clang

- gcc/g++
```bash
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
```
- clang/clang++

```bash
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
```

## Build Release Version

```bash
/usr/bin/cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_C_COMPILER:FILEPATH=$CC -DCMAKE_CXX_COMPILER:FILEPATH=$CXX -S./ -B./release -G "Unix Makefiles"

cd release
make
```

## Build Debug Version

```bash
/usr/bin/cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_C_COMPILER:FILEPATH=$CC -DCMAKE_CXX_COMPILER:FILEPATH=$CXX -S./ -B./build -G "Unix Makefiles"

cd build
make
```

## Run the ui

**IMPORTANT**
Before run the mnsui, Must load the data into shared memory
```bash
shmem <memory key> <tick bin file> <dumped bin file>
```

Please checkout the mns project and run it.
```bash
git clone https://gitee.com/stonego/mns.git
```