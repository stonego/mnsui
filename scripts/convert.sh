#!/bin/bash


run_fix() {
  # pwd
  cd $path
  m4s=$(ls *.m4s)
  if [ ${#m4s} > 1 ] 
  then
    fixm4s $m4s
  fi
  cd ..
}


a=$(ls)

for path in $a
do
  char="${path:0:1}"
  if [ -z $(echo $char | sed -e 's/[0-9]//g') ]
  then
    run_fix path
  elif [ -z $(echo $char | sed  -e 's/[A-Z]//g') ] # find out if character is upper
  then
        echo "$char is UPPER character"
  elif [ -z $(echo $char | sed -e 's/[a-z]//g') ] # find out if character is lower
  then
    echo "$char is lower character"
  else
    echo "$char is Special symbol" # else it is special character
  fi
done
