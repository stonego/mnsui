#!/bin/bash


clean() {
  cd $path
  # pwd
  m4s=$(ls *.m4s)
  if [ ${#m4s} < 1 ] 
  then
    cd ..
    rm -r $path
  fi
}


a=$(ls)

for path in $a
do
  char="${path:0:1}"
  if [ -z $(echo $char | sed -e 's/[0-9]//g') ]
  then
    clean path
  elif [ -z $(echo $char | sed  -e 's/[A-Z]//g') ] # find out if character is upper
  then
        echo "$char is UPPER character"
  elif [ -z $(echo $char | sed -e 's/[a-z]//g') ] # find out if character is lower
  then
    echo "$char is lower character"
  else
    echo "$char is Special symbol" # else it is special character
  fi
done
