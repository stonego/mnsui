#!/bin/sh

# Linux
# COMP_PATH=/usr/bin

# MacOS
# COMP_PATH=/usr/local/Cellar/llvm/17.0.1/bin
COMP_PATH=/usr/local/opt/llvm/bin
INSTALL_PATH=~/usr/bin

if [ $# -lt 1 ]
then
  echo 'Usage: sh scripts/cmake-cmd.sh <release|debug>'
  exit 0
fi

if [[ $1 != 'release' && $1 != 'debug' ]]
then
  echo 'Usage: sh scripts/cmake-cmd.sh <release|debug>'
  exit 0
fi

if [[ $1 == 'release' ]]
then
  BUILD_TYPE=Release
  BUILD_PATH=release
else
  BUILD_TYPE=Debug
  BUILD_PATH=build
fi

# rm -rf $BUILD_PATH
cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=$BUILD_TYPE \
      -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE \
      -DCMAKE_C_COMPILER:FILEPATH=$COMP_PATH/clang \
      -DCMAKE_CXX_COMPILER:FILEPATH=$COMP_PATH/clang++ \
      -S./ -B./$BUILD_PATH -G "Unix Makefiles"

cd $BUILD_PATH
make -j4


if [[ $1 == 'release' ]]
then
  echo 'install release version'
  cp mnsui.app/Contents/MacOS/mnsui $INSTALL_PATH
  cp -rf ../images $INSTALL_PATH
fi

cd ..
