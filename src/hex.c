#include <stdio.h>
#include <ctype.h>
#include <errno.h>

int main(int argc, char** argv) {
  if (argc != 2) {
    printf("%s <input file>\n", argv[0]);
    return (0);
  }

  FILE* f1 = fopen(argv[1], "rb");

  char buf[BUFSIZ];
  size_t n = 0;

  n = fread(buf, 1, 16 * 6, f1);
  if (n != 16 * 6) {
    printf("Error %d\n", errno);
  }

  fclose(f1);

  for (int i = 0; i < 6; ++i) {
    int j = i * 16;
    for (int k = 0; k < 8; ++k) {
      printf("%02x ", buf[j++]);
    }
    printf("  ");
    for (int k = 0; k < 8; ++k) {
     printf("%02x ", buf[j++]);
    }
    printf("  ");
    j = i * 16;
    for (int k = 0; k < 8; ++k) {
      printf("%c", isprint(buf[j]) ? buf[j] : '.');
      ++j;
    }
    printf(" ");
    for (int k = 0; k < 8; ++k) {
      printf("%c", isprint(buf[j]) ? buf[j] : '.');
      ++j;
    }
    printf("\n");
  }

  return (0);
}
