// Copyright (C) 2021 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "datachart.h"
#include "dataview.h"
#include "qgridlayout.h"
#include "types.h"
#include <QLabel>
#include <QSlider>
#include <QTextEdit>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qtablewidget.h>
#include <qtextedit.h>

QT_USE_NAMESPACE

class MainWidget : public QWidget
{
  Q_OBJECT
public:
  explicit MainWidget(QWidget* parent = nullptr);
  ~MainWidget();
  void createUi() noexcept;
  DataView* getDataView() { return m_dataView; }
  DataChart* getDataChart() { return m_chart; }

public Q_SLOTS:
  void showData() noexcept;
  void showDetail() noexcept;

  int togglePosIsTime(bool checked) noexcept;
  int run() noexcept;
  void mouseMove(const int pos, const price_t hpx) noexcept;
  void moveLeft1() noexcept;
  void moveLeft2() noexcept;
  void moveLeft3() noexcept;
  void moveRight1() noexcept;
  void moveRight2() noexcept;
  void moveRight3() noexcept;

  void toggleShowFuture() noexcept;
  void toggleTom() noexcept;
  void toggleJerry() noexcept;
  void toggleMickey() noexcept;
  void toggleMickeyLines() noexcept;


  void received_position(const int64_t pos);
  void received_pet_dir_time(const int pet_id);
  void received_mickey_lines();
  void received_reverse_space();

  void set_pos_by_table_item(QTableWidgetItem* item);

protected:
  void resizeEvent(QResizeEvent*) override;
  void timerEvent(QTimerEvent*) override;
  void closeEvent(QCloseEvent*) override;

  int start_animation_() noexcept;
  int stop_animation_() noexcept;
  int draw__() noexcept;
  int draw_tick_data() noexcept;
  void setPosIndex(const int64_t pos) noexcept;
  void setFrequence(const int frequence) noexcept;

  void movePosDelta(const int delta) noexcept;
  void moveToPos(const int64_t idx) noexcept;

private:
  void show_params__() const noexcept;

private:
  DataChart* m_chart;
  DataView* m_dataView;

  QLabel* m_lblPrice;
  QLabel* m_lblX;
  QLabel* m_lblTimer;
  QTextEdit* m_txtDataIdx;

  QSlider* m_freqSlider;
  QCheckBox* m_togglePosIsTime;

  QPushButton* m_btnShow;
  QPushButton* m_btnDetail;
  QPushButton* m_btnRunOrStop;
  QPushButton* m_btnToggleTom;
  QPushButton* m_btnToggleJerry;
  QPushButton* m_btnToggleMickey;
  QPushButton* m_btnToggleMickeyLines;
  QPushButton* m_btn;
  QPushButton* m_btnLeft_1;
  QPushButton* m_btnLeft_2;
  QPushButton* m_btnLeft_3;
  QPushButton* m_btnRight_1;
  QPushButton* m_btnRight_2;
  QPushButton* m_btnRight_3;
  QPushButton* m_btnFuture;

  QGridLayout* buttonLayout;
  QGridLayout* layoutLR;
  QGridLayout* lotPets;
  QGridLayout* mainLayout;

private:
  bool m_data_changed = false;
  bool m_run = false;
  bool b_pos_is_tm = false;
  int timerId = 0;
  int old_frequence = 0;
  int64_t m_stopPosIndex = 0;
};

#endif // MAINWIDGET_H
