#ifndef DATAVIEW_H
#define DATAVIEW_H

#include "datachart.h"
#include "dirsblocks.h"
#include <QGraphicsView>
#include <QTableWidget>
#include <qtablewidget.h>
#include <qtmetamacros.h>

class DataView : public QGraphicsView
{
  Q_OBJECT
public:
  DataView(DataChart* chart, QWidget* parent /* = nullptr */);
  ~DataView();

  QTableWidget* getTable() noexcept { return m_table; }
protected:
  void resizeEvent(QResizeEvent* event) override;

public Q_SLOTS:
  void rececived_action(const ActionPtr& action);
  void clear_action();

private:
  DataChart* m_chart = nullptr;
  DirsBlocks* m_dirs = nullptr;
  QTableWidget* m_table = nullptr;
};

#endif // DATAVIEW_H
