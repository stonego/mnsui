#ifndef DIRSBLOCKS_H
#define DIRSBLOCKS_H

#include "datachart.h"
#include <QGraphicsItem>

class DirsBlocks : public QGraphicsItem
{
public:
  DirsBlocks(DataChart* chart);

  void updateGeometry();
  void paint(QPainter* painter,
             const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;
  QRectF boundingRect() const override;

private:
  QPointF m_anchor;
  DataChart* m_chart = nullptr;

  QPen pen;
  QBrush brush;

  QImage imgPet[3];

  void draw_image_(QPainter* painter, const int idx_pet) noexcept;
  void draw_opportunities(QPainter* painter, const Trade::Params* p) noexcept;
  void draw_blocks(QPainter* painter, const Trade::Params* p) noexcept;
};

#endif // DIRSBLOCKS_H
