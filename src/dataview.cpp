#include "dataview.h"
#include "dirsblocks.h"
#include <QHeaderView>
#include <QTableWidget>
#include <QWidget>

DataView::DataView(DataChart* chart, QWidget* parent)
  : QGraphicsView(new QGraphicsScene, parent)
  , m_chart(chart)
{
  setDragMode(QGraphicsView::NoDrag);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setFrameShape(QFrame::NoFrame);
  setBackgroundRole(QPalette::Window);
  setRenderHint(QPainter::Antialiasing);
  setMouseTracking(true);

  m_dirs = new DirsBlocks(chart);
  m_table = new QTableWidget(this);
  m_table->setColumnCount(5);
  m_table->setRowCount(0);
  m_table->verticalHeader()->hide();
  m_table->horizontalHeader()->hide();
  m_table->setColumnWidth(0, 70);
  m_table->setColumnWidth(1, 50);
  m_table->setColumnWidth(2, 17);
  m_table->setColumnWidth(3, 80);
  m_table->setColumnWidth(4, 50);

  m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

  scene()->addItem(m_chart);
}

DataView::~DataView()
{
  delete m_dirs;
  m_dirs = nullptr;
  delete scene();
}

void
DataView::resizeEvent(QResizeEvent* event)
{
  if (scene()) {
    scene()->setSceneRect(QRect(QPoint(0, 0), event->size()));
    m_chart->resize(event->size());

    auto r = rect();
    m_table->setGeometry(88, r.bottom() - 158, 272, 100);
  }
  resize(size());
}

void
DataView::clear_action()
{
  m_table->clear();
}

void
DataView::rececived_action(const ActionPtr& action)
{
  if (m_table->rowCount() > 39) {
    m_table->removeRow(39);
  }
  m_table->insertRow(0);

  QTableWidgetItem* item = new QTableWidgetItem(action->name);
  m_table->setItem(0, 0, item);

  item = new QTableWidgetItem(action->act);
  m_table->setItem(0, 1, item);

  char buf[64];
  snprintf(buf, 64, "%.d", action->d);
  item = new QTableWidgetItem(buf);
  m_table->setItem(0, 2, item);

  snprintf(buf, 64, "%.02lf", action->hpx);
  item = new QTableWidgetItem(buf);
  m_table->setItem(0, 3, item);

  snprintf(buf, 64, "%d", action->vol);
  item = new QTableWidgetItem(buf);
  m_table->setItem(0, 4, item);
}
