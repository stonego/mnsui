#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

  extern void* udp_main(void* param);

  extern void close_udp_server();

#ifdef __cplusplus
}
#endif
