#pragma once

#include "tick.h"
#include <stddef.h>
#include <stdint.h>
#include <time.h>

#ifdef __cplusplus
extern "C"
{
#endif

  extern int get_frequence_by_id(const int idx);
  extern int down_sampler(const Tick* tick,
                          const size_t tick_size,
                          const int down_interval,
                          const int64_t pos,
                          Tick* data,
                          const int target_len, /* in and out */
                          int8_t direction);
  extern Tick* load_all_ticker(const char* filepath, size_t* size);
  extern Tick* load_ticker_bin(const char* filepath,
                               size_t* size,
                               const int64_t spos,
                               const int64_t epos);
  extern int64_t find_time(const Tick* tick,
                           const size_t tick_size,
                           const time_t tm);

#ifdef __cplusplus
}
#endif
