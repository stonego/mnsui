#ifndef DATACHART_H
#define DATACHART_H

#include "data.h"
#include <QChart>
#include <QGraphicsSceneMouseEvent>
#include <QMouseEvent>
#include <QObject>
#include <QString>
#include <QWheelEvent>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <lines.hpp>
#include <params.hpp>
#include <qlineseries.h>
#include <qtmetamacros.h>

class DataChart : public QChart
{
  Q_OBJECT
public:
  DataChart();

public:
  using QChart::QChart;

private:
  qreal minY_ = 0;
  qreal maxY_ = 0;

public:
  void draw() noexcept;
  void setSeries() noexcept;
  void setSeries(const int historyLen,
                 const int futureLen,
                 const int interval,
                 const Tick* history,
                 const Tick* future) noexcept;
  void setTimerSeries(const int64_t pos) noexcept;
  void setPetTimer(const int pet_id) noexcept;
  void setMickLines() noexcept;
  QString getTimerString() const noexcept;
  void resetY();
  void updateY();

Q_SIGNALS:
  void mouseMove(const int pos, const price_t hpx);

public:
  void visibleTom() noexcept;
  void visibleJerry() noexcept;
  void visibleMickey() noexcept;
  void visibleMickeyLines() noexcept;

private:
  void updatePetTimer(QLineSeries* s1,
                      QLineSeries* s2,
                      int pet_id) noexcept;

protected:
  void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

  int sampler_data() noexcept;

private:
  QLineSeries* m_seriesHistory;
  QLineSeries* m_seriesFuture;

  QLineSeries* m_seriesU1;
  QLineSeries* m_seriesU2;
  QLineSeries* m_seriesD1;
  QLineSeries* m_seriesD2;

  QLineSeries* m_mickeyU1;
  QLineSeries* m_mickeyU2;
  QLineSeries* m_mickeyD1;
  QLineSeries* m_mickeyD2;

  QLineSeries* m_extra_up;
  QLineSeries* m_extra_dn;

  QLineSeries* m_tm_near;
  QLineSeries* m_tm_far;

  QLineSeries* m_pet_t1[3];
  QLineSeries* m_pet_t2[3];

  QValueAxis* m_axisY;
  QValueAxis* m_axisX;

  int m_historyLen = 0;
  int m_futureLen = 0;
  Tick history[DATA_LENGTH];
  Tick future[FUTURE_LENGTH];
};

#endif // DATACHART_H
